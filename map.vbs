Const PAGE_SIZE = 8192

Dim fso, files
Set fso = WScript.CreateObject("Scripting.Filesystemobject")
Dim oFSO
Set oFSO0 = fso.CreateTextFile("index0.asm")
Set oFSO1 = fso.CreateTextFile("index1.asm")
Set oFSO2 = fso.CreateTextFile("index2.asm")

set oFSOd = fso.CreateTextFile("romData.asm")


Set files = fso.GetFolder(".").Files
Dim page 
Dim offset 
Dim padding

page = 0
offset = 0
disc = 0
sector = 0
padding = 0
opllpatch = 0

oFSO0.WriteLine  "romIndex0:"
oFSO1.WriteLine  "romIndex1:"
oFSO2.WriteLine  "romIndex2:"
oFSOd.WriteLine "romData:"

For Each f In files

	If LCase(Right(f.Name,4))=".pck" Then

		If InStr(f.Name,"aleste21") Then
			disc = 0
		Else
			If InStr(f.Name,"aleste22") Then
				disc = 1
			Else
				disc = 2
			End If
		End If
		sector = "#" + Mid(f.Name,InStr(F.Name,"_")+1,3)
		opllpatch = Mid(f.Name,InStr(F.Name,".")-1,1)
		
		If offset >= PAGE_SIZE Then
			page = page + 1
			offset = offset - PAGE_SIZE
		End If
	
		If f.Size < (PAGE_SIZE - offset + PAGE_SIZE) Then
			'fits
			writeIndex(disc)
			offset = offset + f.Size

		Else
			'does not fit, pad and skip to next page
			padding = 8192 - offset
			oFSOd.WriteLine vbTab & "ds	" & padding
			
			offset = 0
			page = page + 1

			writeIndex(disc)
			offset = offset + f.Size
		End If
		'write incbin
		oFSOd.WriteLine vbTab & "incbin" & vbTab & f.Name
		
	End If

Next
str = vbTab & "dw" & vbTab & "#ffff"
oFSO0.WriteLine str
oFSO1.WriteLine str
oFSO2.WriteLine str

Sub writeIndex (disc)
	Dim str
	str = vbTab & "dw" & vbTab & sector & vbCrLf & vbTab & "db" & vbTab & page & vbCrLf & vbTab & "dw" & vbTab & offset & vbCrLf & vbTab & "db" & vbTab & opllpatch  & vbCrLf
	Select Case disc
		case 0
			oFSO0.WriteLine str
		case 1
			oFSO1.WriteLine str
		case 2
			oFSO2.WriteLine str			
	End Select

End Sub